/**
 * Let's add the hide button behavior
 */
Drupal.behaviors.aef_formatter_selector_behavior = function() {
  $('.composite-cck-preview').each(function() {
    var preview = $(this);

    if(preview.hasClass('composite-cck-preview-processed') == false)
    {
      $('a', preview).click(function() {
        preview.hide('normal');
        return false;
       });
      preview.addClass('composite-cck-preview-processed');
    }
  });
}