AEF Formatter Selector
======================
This module allows you to choose a CCK formatter (a kind of theme) in the node edit page. Please note that while it may work on any field type, it has only been tested and heavily used on Nodereference!

Configuration
-------------
On a given content type, create a nodereference "Autocomplete text field". On the field edit page, you will find a fieldset called "Formatter selector options".
Select "Users can choose the individual formatters of the field", select the formatters you want the user to be able to choose, and select the default selected formatter. And that's it!

How to create Node formatters
-----------------------------
A CCK formatter is a standard feature given by CCK2, which is more or less a theme of a field that can be easily reused. Heavy use of this feature can save you a lot of time for big sites!!
To create one formatter, here are the guidelines:

1) Create a formatter, implementing the hook_field_formatter_info()
function aef_ct_article_field_formatter_info() {
	$formatters = array(
		'aef_ct_article_teaser_1' => array(
			'label' => t('Article: Teaser, image on the left'),
			'field types' => array('nodereference'),
			'description' => t('Displays a teaser with the main image on the left.'),
		),
  );
  return $formatters;
}

2) Create the theme functions, implementing the hook_theme()
function aef_ct_article_theme($existing, $type, $theme, $path)
{
	return array(
		'aef_ct_article_formatter_aef_ct_article_teaser_1' => array(
			'arguments' => array('element' => null),
		),
		'aef_ct_article_teaser_1' => array(
			'arguments' => array('node' => null),
      'template' => 'aef_ct_article_teaser_1',
      'path' => drupal_get_path('module', 'aef_ct_article') . '/theme',
		),
  );
}
with aef_ct_article_formatter_aef_ct_article_teaser_1 being called by the formatter ([module_name]_formatter_[formatter_name]).
This theme function is basically a wrapper around the second theme function:
function theme_aef_ct_article_formatter_aef_ct_article_teaser_1($element) {

	return aef_ct_load_and_call_theme('aef_ct_article_teaser_1', $element);
}
FYI, the code of aef_ct_load_and_call_theme is given at the end of the README

3)Implement the template, here aef_ct_article_teaser_1.tpl.php located in drupal_get_path('module', 'aef_ct_article') . '/theme'




Advanced features: Formatter list according to the node
-------------------------------------------------------
You may create CCK formatters specific to one content type (E.g. a formatter for video content type only), and you want this formatter not to be selectable if a non-video node is in the nodereference.
That's simple! Just add two lines in your formatter declaration:
	$formatters = array(
		'aef_ct_article_teaser_1' => array(
			'label' => t('Article: Teaser, image on the left'),
			'field types' => array('nodereference'),
			'description' => t('Displays a teaser with the main image on the left.'),
      'suitability callback' => 'aef_ct_is_formatter_suitable',
      'suitability callback arguments' => array('type' => 'article'),
		),
and Formatter selector will check the inserted node in the nodereference against the suitability callback, and return formatters that pass the test. The aef_ct_is_formatter_suitable code is given below, and is part of another module that will be released.
















function aef_ct_load_and_call_theme($theme_name, $element) {
  // Inside a View this function may be called with null data.  In that case,
  // just return.
  if (empty($element['#item'])) {
    return '';
  }

  $html = "";

  if ($node = node_load($element['#item']['nid'])) {
    $node->build_mode = 'teaser';
    //Not a good idea at all!!! Perf decrease of 3000%
    //node_view($node);
    $html = theme($theme_name, $node);
  }

  return $html;
}



function aef_ct_is_formatter_suitable($value, $args = array()) {
  //Only check the first value, for now
  if (isset($value[0])) {
    $value = $value[0];
  }

  if (is_array($value)) {
    if (is_numeric($value['nid'])) {
      $nid = $value['nid'];
    }
    else if (is_array($value['nid']['nid'])) {
      preg_match('/^(?:\s*|(.*) )?\[\s*nid\s*:\s*(\d+)\s*\]$/', $value['nid']['nid']['#value'], $matches);
      $nid = $matches[2];
    }
    else if (!empty($value['nid']['nid'])) {
      preg_match('/^(?:\s*|(.*) )?\[\s*nid\s*:\s*(\d+)\s*\]$/', $value['nid']['nid'], $matches);
      $nid = $matches[2];
    }
  }

  if ($node = node_load($nid)) {
    if ($node->type == $args['type']) {
      return TRUE;
    }
  }

  return FALSE;
}
